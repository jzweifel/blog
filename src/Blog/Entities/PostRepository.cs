﻿using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Entities
{
    public class PostRepository : IPostRepository
    {
        private readonly IBlogContext _context;

        public PostRepository(IBlogContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Post>> GetAll()
        {
            return await _context.Posts.ToListAsync();
        }

        public  void Update(int id, Post post)
        {
            var dbPost = Get(id);
            dbPost.Title = post.Title;
            dbPost.Content = post.Content;

            _context.Update(dbPost);
        }

        public bool Delete(int id)
        {
            var dbPost = Get(id);
            if (dbPost == null) return false;
            _context.Remove(dbPost);
            return true;

        }

        public Post Get(int id)
        {
            return _context.Posts.SingleOrDefault(x => x.Id == id);
        }

        public void Add(Post post)
        {
            _context.Add(post);
        }

        public async Task<bool> Commit()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}