﻿using Microsoft.Data.Entity;

namespace Blog.Entities
{
    public class BlogContext : DbContext, IBlogContext
    {
        public DbSet<Post> Posts { get; set; }

        public BlogContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Startup.Configuration["Data:DefaultConnection:ConnectionString"]);

            base.OnConfiguring(optionsBuilder);
        }
    }
}
