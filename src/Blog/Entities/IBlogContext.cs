﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.ChangeTracking;

namespace Blog.Entities
{
    public interface IBlogContext
    { 
        DbSet<Post> Posts { get; set; }

        EntityEntry Add(object entity, GraphBehavior behavior = GraphBehavior.IncludeDependents);
        EntityEntry Update(object entity, GraphBehavior behavior = GraphBehavior.IncludeDependents);
        EntityEntry Remove(object entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken());
    }
}