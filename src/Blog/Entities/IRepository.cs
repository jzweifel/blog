﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blog.Entities
{
    public interface IRepository<T>
    {
        void Add(T item);
        void Update(int id, T item);
        bool Delete(int id);
        T Get(int id);
        Task<IEnumerable<Post>> GetAll();

        Task<bool> Commit();
    }
}