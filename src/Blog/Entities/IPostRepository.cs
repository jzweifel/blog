﻿namespace Blog.Entities
{
    public interface IPostRepository : IRepository<Post> { }
}