using Microsoft.Data.Entity.Migrations;

namespace Blog.Migrations
{
    public partial class Post_RenameBodyToContent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "Body", table: "Post");
            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Post",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "Content", table: "Post");
            migrationBuilder.AddColumn<string>(
                name: "Body",
                table: "Post",
                nullable: true);
        }
    }
}
