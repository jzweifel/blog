using System;
using System.ComponentModel.DataAnnotations;

namespace Blog.ViewModels
{
    public class PostViewModel
    {
        [Required]
        [StringLength(255, MinimumLength = 5)]
        public string Title { get; set; }

        public DateTime Created { get; set; }

        [Required]
        public string Content { get; set; }
    }
}