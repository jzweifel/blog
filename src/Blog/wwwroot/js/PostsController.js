﻿(function() {
    "use strict";

    angular.module("app-admin")
        .controller("postsController", postsController);

    function postsController($http) {
        var vm = this;

        vm.posts = [
            {
                id: 0,
                title: "",
                created: ""
            }
        ];
        vm.errorMessage = "";

        var baseUrl = "/api/posts";

        vm.deletePost = function(index) {
            $http.delete(baseUrl + "/" + vm.posts[index].id).then(function() {
                vm.posts.splice(index, 1);
            }, function(error) {
                vm.errorMessage = "Failed to delete post: " + error.statusText;
            });
        };

        $http.get(baseUrl).then(function (response) {
            angular.copy(response.data, vm.posts);
        }, function(error) {
            vm.errorMessage = "Failed to load posts: " + error.statusText;
        });
    };
})();