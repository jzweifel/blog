﻿(function() {
    "use strict";

    angular.module("app-admin")
        .controller("editPostController", editPostController);

    function editPostController($routeParams, $http, $location) {
        var vm = this;
       
        vm.post = {
            id: $routeParams.postId,
            title: "",
            content: ""
        };

        vm.errorMessage = "";

        var endpoint = vm.post.id ? "/api/posts/" + vm.post.id : "/api/posts";
        
        vm.updatePost = function() {
            $http.put(endpoint, vm.post)
                .then(function() {
                    $location.path("/");
                }, function (error) {
                    vm.errorMessage = "Failed to save post: " + error.statusText;
                });
        };

        vm.addPost = function () {
            $http.post(endpoint, vm.post)
                .then(function () {
                    $location.path("/");
                }, function (error) {
                    vm.errorMessage = "Failed to save post: " + error.statusText;
                });
        };

        $http.get(endpoint)
            .then(function(response) {
                angular.copy(response.data, vm.post);
            }, function(error) {
                vm.errorMessage = "Failed to load post: " + error.statusText;
            });
    };
})();