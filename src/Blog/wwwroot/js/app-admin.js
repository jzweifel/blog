﻿(function () {
    "use strict";
    angular.module("app-admin", ["ngRoute", "ngQuill", "angular-loading-bar"])
        .config(function ($routeProvider) {
            $routeProvider.when("/", {
                controller: "postsController",
                controllerAs: "vm",
                templateUrl: "/views/PostsView.html"
            });

            $routeProvider.when("/editor/:postId", {
                controller: "editPostController",
                controllerAs: "vm",
                templateUrl: "/views/EditPostView.html"
            });

            $routeProvider.when("/editor", {
                controller: "editPostController",
                controllerAs: "vm",
                templateUrl: "/views/EditPostView.html"
            });

            $routeProvider.otherwise({ redirectTo: "/ " });
        })
    .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }]);
})();