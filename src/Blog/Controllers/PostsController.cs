﻿using System;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Blog.Entities;
using Blog.ViewModels;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Razor.TagHelpers;

namespace Blog.Controllers
{
    [Route("api/posts")]
    public class PostsController : Controller
    {
        private readonly IPostRepository _postRepository;

        public PostsController(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        [HttpGet]
        public async Task<JsonResult> Get()
        {
            var posts = await _postRepository.GetAll();
            return Json(posts);
        }

        [HttpGet]
        [Route("{id}")]
        public JsonResult Get(int id)
        {
            var post = _postRepository.Get(id);
            return Json(post);
        }

        [HttpPost]
        public async Task<JsonResult> Post([FromBody] PostViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var post = Mapper.Map<Post>(vm);
                post.Created = DateTime.UtcNow;
                _postRepository.Add(post);

                if (await _postRepository.Commit())
                {
                    Response.StatusCode = (int)HttpStatusCode.Created;
                    return Json(Mapper.Map<PostViewModel>(post));
                }
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Invalid post submitted" });
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<JsonResult> Put(int id, [FromBody] PostViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var userPost = Mapper.Map<Post>(vm);

                _postRepository.Update(id, userPost);

                if (await _postRepository.Commit())
                {
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return Json(new { });
                }
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Invalid post submitted" });

        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<JsonResult> Delete(int id)
        {
            if (_postRepository.Delete(id))
            {
                if (await _postRepository.Commit())
                {
                    Response.StatusCode = (int) HttpStatusCode.NoContent;
                    return Json(new {});
                }
            }

            Response.StatusCode = (int) HttpStatusCode.NotFound;
            return Json(new {});
        }
    }
}